Implementation of RNN/CRF-based models using Pytorch 
====================================================
The ``src`` folder contains implementation of recurrent neural network (RNN) models with multiple variations including conditional random field (CRF) layer with unary and pairwise potential functions.


Recurrent Neural Network (RNN)
------------------------------

The ``RNN_Labeler`` model can be constructed based on LSTM or GRU (i.e. gated-cells) and adds support for embedding layer and intermediate layer that maps
from the RNN output to the tagspace output.

-------------------------------------------

	
RNNSS: RNN with scheduled sampling (i.e. teacher forcing)
----------------------------------------------------------

The ``RNNSS_Labeler`` is similar to ``RNN_Labeler`` model but adds support for using previous label at each timestep to predict the current label using
scheduled sampling (i.e. teacher forcing approach, see `Bengio et al. paper <https://arxiv.org/abs/1506.03099>`__)

-------------------------------------------

	   
RNNCRF with Unary potential
------------------------------

The ``RNNCRFUnary_Labeler`` is built on the RNN model where the loss function is based on the log conditional probability of a sequence (:math:`log(P(Y|X)`) rather than
the cross-entropy of each label at each timestep. It adds a conditional random field layer that uses unary potential :math:`\phi(x_t, y_t)` with transition parameters :math:`A(y_{t-1}, y_t)` for computing
the scores at each timestep.

It supports multiple training schemes:

	- Structured perceptron
	- Maximum likelihood (MLE) and  maximum a posteriori (MAP) 
	
and multiple decoding schemes:

	- Viterbi
	- Guided Viterbi (i.e. decoding only the last event vector -- case of classification of last event)
	- Posterior probability decoding (i.e. based on marginal probabilities of each label at each timestep)

TODO:

	- Implement top-K decoding using A* search 
	- Support beam-search decoding
	- Add support for violation-fixing framework 
	 
-------------------------------------------

	
RNNCRF with Pairwise potential
------------------------------

The ``RNNCRF_Labeler`` is similar to ``RNNCRUnary_Labeler`` but it supports pairwise potentials in addition to transition parameters (:math:`\phi(x_t, y_{t-1}, y_t)` with transition parameters :math:`A(y_{t-1}, y_t)`). Hence, it has much more parameters to estimate.

It supports multiple training schemes:

	- Structured perceptron
	- Maximum likelihood (MLE) and  maximum a posteriori (MAP) 
	
and multiple decoding schemes:

	- Viterbi
	- Guided Viterbi (i.e. decoding only last event vector)
	- Posterior probability decoding (i.e. based on marginal probabilities of each label at each timestep)

TODO:

	- Implement top-K decoding using A* search 
	- Support beam-search decoding
	- Add support for violation-fixing framework 

-------------------------------------------


License
--------
MIT License file accompanies this repository.