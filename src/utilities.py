'''
@author: ahmed allam <ahmed.allam@nih.gov>
'''
import os
import pickle
import numpy as np
import torch

current_dir = os.path.dirname(os.path.realpath(__file__))
project_dir = os.path.abspath(os.path.join(current_dir, os.pardir))

PADDSYMB_INDX = 1000
IS_CUDA = torch.cuda.is_available()


def get_tensordtypes(to_gpu):
    fdtype = torch.FloatTensor
    ldtype = torch.LongTensor
    bdtype = torch.ByteTensor
    if(IS_CUDA and to_gpu):
        fdtype = torch.cuda.FloatTensor
        ldtype = torch.cuda.LongTensor
        bdtype = torch.cuda.ByteTensor
    return(fdtype,ldtype, bdtype)

def argmax_var(var, dim=0):
    """ return the index of maximum value in a variable as a scalar
    """
    __, maxscore_indx = torch.max(var, dim=dim)
    return(maxscore_indx.data[0])

def logsumexp_var(var, dim=0):
    max_score = var[argmax_var(var, dim)]
    return(max_score + torch.log(torch.sum(torch.exp(var - max_score))))

def compute_numtrials(prob_interval_truemax, prob_estim):
    """ computes number of trials needed for random hyperparameter search 
        see `algorithms for hyperparameter optimization paper 
             <https://papers.nips.cc/paper/4443-algorithms-for-hyper-parameter-optimization.pdf>`__
    """
    n = np.log(1-prob_estim)/np.log(1-prob_interval_truemax)
    return(int(np.ceil(n))+1)

class ReaderWriter(object):
    """class for dumping, reading and logging data"""
    def __init__(self):
        pass
    @staticmethod
    def dump_data(data, file_name, mode = "wb"):
        """dump data by pickling 
        
           Args:
               data: data to be pickled
               file_name: file path where data will be dumped
               mode: specify writing options i.e. binary or unicode
        """
        with open(file_name, mode) as f:
            pickle.dump(data, f) 
    @staticmethod  
    def read_data(file_name, mode = "rb"):
        """read dumped/pickled data
        
           Args:
               file_name: file path where data will be dumped
               mode: specify writing options i.e. binary or unicode
        """
        with open(file_name, mode) as f:
            data = pickle.load(f)
        return(data)
    
    @staticmethod
    def write_log(line, outfile, mode="a"):
        """write data to a file
        
           Args:
               line: string representing data to be written out
               outfile: file path where data will be written/logged
               mode: specify writing options i.e. append, write
        """
        with open(outfile, mode) as f:
            f.write(line)
    @staticmethod
    def read_log(file_name, mode="r"):
        """write data to a file
        
           Args:
               line: string representing data to be written out
               outfile: file path where data will be written/logged
               mode: specify writing options i.e. append, write
        """
        with open(file_name, mode) as f:
            for line in f:
                yield line
                
def create_directory(folder_name, directory = "current"):
    """create directory/folder (if it does not exist) and returns the path of the directory
    
       Args:
           folder_name: string representing the name of the folder to be created
       
       Keyword Arguments:
           directory: string representing the directory where to create the folder
                      if `current` then the folder will be created in the current directory
    """
    if directory == "current":
        path_current_dir = os.path.dirname(__file__)
    else:
        path_current_dir = directory
    path_new_dir = os.path.join(path_current_dir, folder_name)
    if not os.path.exists(path_new_dir):
        os.makedirs(path_new_dir)
    return(path_new_dir)



def scheduled_sampling(mode, numbatches_per_epoch, num_epochs, **kwargs):
    max_ep = kwargs.get('max_epsilon', 0.99)
    min_ep = kwargs.get('min_epsilon', 0.2)
    damp_factor = kwargs.get('damping_factor', 2)
    if(mode == 'linear'):
        # compute slope
        slope = (min_ep *(1/damp_factor) - max_ep)/(num_epochs*numbatches_per_epoch-1)
        intercept = max_ep
        def linear_schedule(ibatch):
            return(slope*ibatch+intercept)
        return(linear_schedule)
    elif(mode == 'exponential'):
        # y=ae^{bx}
        b = (np.log(min_ep/max_ep))/((num_epochs*numbatches_per_epoch-1))
        def exponential_schedule(ibatch):
            return(max_ep*np.exp(damp_factor*b*ibatch))
        return(exponential_schedule)
    elif(mode == 'sigmoid'):
        # y = eta/(eta+aexp(bx))
        eta = kwargs.get('eta', 1)
        a = (1-max_ep)*eta/max_ep
        b = np.log(((1-min_ep)*eta)/(a*min_ep))/(num_epochs*numbatches_per_epoch-1)
        def sigmoid_schedule(ibatch):
            return(eta/(eta+a*np.exp(damp_factor*b*ibatch)))
        return(sigmoid_schedule)
    

if __name__ == "__main__":
    pass
    