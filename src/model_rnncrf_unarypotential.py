'''
@author: ahmed allam <ahmed.allam@nih.gov>
'''
import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
from utilities import logsumexp_var, argmax_var, get_tensordtypes
 
class RNNCRFUnary_Labeler(nn.Module):
    def __init__(self, input_dim, hidden_dim,  y_codebook,
                 embed_dim=0, interm_dim=0, num_hiddenlayers=1, 
                 bidirection= False, pdropout=0., rnn_class=nn.LSTM, 
                 nonlinear_func=F.relu, startstate_symb="__START__", stopstate_symb="__STOP__", 
                 to_gpu=True):
        
        super(RNNCRFUnary_Labeler, self).__init__()
        self.fdtype, self.ldtype, __ = get_tensordtypes(to_gpu)
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.tagset_size = len(y_codebook)
        self.y_codebook = y_codebook
        self.num_hiddenlayers = num_hiddenlayers
        self.pdropout = pdropout
        self.neginf = -100000
        if(embed_dim): # embed layer
            self.embed = nn.Linear(self.input_dim, embed_dim)
            self.rnninput_dim = embed_dim
        else:
            self.embed = None
            self.rnninput_dim = self.input_dim
        self.rnn = rnn_class(self.rnninput_dim, hidden_dim, num_layers= num_hiddenlayers, 
                             dropout=pdropout, bidirectional=bidirection, batch_first=True)
        if(bidirection):
            self.num_directions = 2
        else:
            self.num_directions = 1

        if(interm_dim): 
            # intermediate layer between hidden and tag layer
            self.intermlayer = nn.Linear(self.num_directions*hidden_dim, interm_dim) 
            self.hiddenTotag = nn.Linear(interm_dim, self.tagset_size)
        else:
            self.intermlayer = None
            self.hiddenTotag = nn.Linear(self.num_directions*hidden_dim, self.tagset_size)
        self.nonlinear_func = nonlinear_func       
        # transition parameters
        # y_transparams{i,j} indicates the score of transitioning from tag i to tag j
        self.y_transparams = torch.randn(self.tagset_size, self.tagset_size).type(self.fdtype)
        self._check_ycodebook(startstate_symb, stopstate_symb)

    def _check_ycodebook(self, startstate_symb, stopstate_symb):
        self.startstate_symb = startstate_symb
        self.stopstate_symb = stopstate_symb
        y_codebook = self.y_codebook
        for symb in [startstate_symb, stopstate_symb]:
            if(symb not in y_codebook):
                raise(Exception('START and STOP states/symbols must be defined in y_codebook'))
        self.y_transparams[:, y_codebook[startstate_symb]] = self.neginf
        self.y_transparams[y_codebook[stopstate_symb], :] = self.neginf
        self.y_transparams = nn.Parameter(self.y_transparams)
        # setup inverse y_codebook
        self.y_codebookrev = {code:label for label, code in self.y_codebook.items()}
#         print("y_codebook: \n", self.y_codebook)
#         print("transitions: \n", self.y_transparams)

    def init_hidden(self, batch_size):
        """initialize hidden vectors at t=0
        
        Args:
            batch_size: int, the size of the current evaluated batch
        """
        # a hidden vector has the shape (num_layers*num_directions, batch, hidden_dim)
        h0=autograd.Variable(torch.zeros(self.num_hiddenlayers*self.num_directions, batch_size, self.hidden_dim)).type(self.fdtype)
        if(isinstance(self.rnn, nn.LSTM)):
            c0=autograd.Variable(torch.zeros(self.num_hiddenlayers*self.num_directions, batch_size, self.hidden_dim)).type(self.fdtype)
            hiddenvec = (h0,c0)
        else:
            hiddenvec = h0
        return(hiddenvec)

#         
    def compute_potential(self, seq):
        """ compute unary potential \phi(x_t, y_t)
            
            Args:
                seq: sequence, torch variable of shape (batch,seqlen,input_dim)
                
        """
        if(self.embed): # in case an embed layer is defined
            seq = self.nonlinear_func(self.embed(seq))
        hidden = self.init_hidden(seq.size(0)) # initialize hidden vectors by sending the batch size
        rnnout, hidden = self.rnn(seq, hidden)

        if(self.intermlayer): # in case intermediate layer is defined
            intermres = self.nonlinear_func(self.intermlayer(rnnout))
            feat = self.hiddenTotag(intermres).view(-1,self.tagset_size)
                
        else:
            feat = self.hiddenTotag(rnnout).view(-1,self.tagset_size)
        # feat will have a shape (seqlen, tagset_size)
#         print("feat: \n", feat)
#         print("hidden: \n", self.hidden)
        return(feat)
    
    def compute_forward_vec(self, seq_potential):
        """ compute alpha matrix
            
            Args:
                seq_potential: computed potential at every time step using :func:`self.compute_potential`. 
                               It has the shape (seqlen,tagset_size) 
            
            TODO:
                since unary potentials are used, this function could be sped up by using tensor multiplication
                and logsumexp applied on the whole result rather than looping over each tag...
                It is currently kept in this form to be similar to the pairwise potential approach in :class:`RNNCRF_Labeler` class
        """
#         print("we are in compute_forward_vec")
        y_codebook = self.y_codebook
        T = seq_potential.size(0) # sequence length
        num_rows = T+2
            
        # create alpha matrix
        alpha = torch.Tensor(num_rows, self.tagset_size).fill_(self.neginf).type(self.fdtype)
        alpha[0, y_codebook[self.startstate_symb]] = 0.
        alpha = autograd.Variable(alpha)
#         print("alpha matrix: \n", alpha)
        for t in range(T):
#             print("t: ", t)
            for curr_tag in range(self.tagset_size):
#                 print("curr_tag: ", curr_tag)
                emit_score = seq_potential[t, curr_tag]
#                 print("emit_score \n", emit_score)
                trans_score = self.y_transparams[:, curr_tag]
#                 print("trans_score: \n", trans_score)
#                 print("alpha[{},:]: \n {}".format(t, alpha[t,:]))
                curr_tag_var = alpha[t,:] + trans_score  + emit_score 
                alpha[t+1, curr_tag] = logsumexp_var(curr_tag_var) 
#                 print("alpha[{},{}]: \n {}".format(t+1, curr_tag, alpha[t+1, curr_tag]))
            
        curr_tag = y_codebook[self.stopstate_symb] # stop state
        curr_tag_var = alpha[num_rows-2,:] + self.y_transparams[:,curr_tag]
        alpha[num_rows-1, curr_tag] = logsumexp_var(curr_tag_var)
        Z = alpha[num_rows-1, curr_tag]
#         print("alpha: \n", alpha)
#         print("Z: ", Z)
        return(Z, alpha)
    
    def compute_backward_vec(self, seq_potential):
        """ compute beta matrix
        
            Args:
                seq_potential: computed potential at every time step using :func:`self.compute_potential`. 
                               It has the shape (seqlen,tagset_size)
                     
        """
#         print("we are in compute_forward_vec")
        y_codebook = self.y_codebook
        T = seq_potential.size(0) # sequence length
        num_rows = T+2
            
        # create beta matrix
        beta = torch.Tensor(num_rows, self.tagset_size).fill_(self.neginf).type(self.fdtype)
        beta[num_rows-1, y_codebook[self.stopstate_symb]] = 0.
        beta = autograd.Variable(beta)
#         print("beta matrix: \n", beta)
        
        for t in reversed(range(1, num_rows-1)):
#             print("t: ", t)
            for curr_tag in range(self.tagset_size):
#                 print("curr_tag: ", curr_tag)
                if(t < T):
                    emit_score = seq_potential[t, :]
                    trans_score = self.y_transparams[curr_tag, :]
                    curr_tag_var = beta[t+1,:] + trans_score + emit_score
#                     print("emit_score: \n", emit_score)
                else: # t == T
                    trans_score = self.y_transparams[curr_tag, :]
                    curr_tag_var = beta[t+1,:] + trans_score         
       
#                 print("trans_score: \n", trans_score)
#                 print("beta[{},:]: \n {}".format(t, beta[t,:]))
                beta[t, curr_tag] = logsumexp_var(curr_tag_var) 
#                 print("beta[{},{}]: \n {}".format(t+1, curr_tag, beta[t+1, curr_tag]))
            
        curr_tag = y_codebook[self.startstate_symb] # start state
        curr_tag_var = beta[1,:] + self.y_transparams[curr_tag, :] + seq_potential[0, :]
        beta[0, curr_tag] = logsumexp_var(curr_tag_var)
        Z = beta[0, curr_tag]
#         print("beta: \n", beta)
#         print("Z: ", Z)
        return(Z, beta)
    
    def compare_alpha_beta(self, seq_potential):
        """sanity check function for comparing the alpha/beta computation 
        """
        Z_alpha, alpha = self.compute_forward_vec(seq_potential)
        print("alpha: \n", alpha)
        print("Z_alpha: ", Z_alpha)
        Z_beta, beta = self.compute_backward_vec(seq_potential)
        print("beta: \n", beta)
        print("Z_beta: ", Z_beta)  
        print("abs diff: ", torch.abs(Z_alpha-Z_beta))
        self.compute_marginal_prob(alpha, beta, Z_alpha)
        
    def compute_marginal_prob(self, alpha, beta, Z):        
        """compute the marginal probability of every state at each timestep
        """
        # create marginal probability matrix
        P_marginal = alpha + beta - Z
        P_marginal = torch.exp(P_marginal)
        print("P_marginal: \n", P_marginal)    
        print("Sum P_marginal: \n", torch.sum(P_marginal, dim=1))
        return(P_marginal)

    
    def compute_refscore(self, seq_potential, y_labels):
        """ computes the probability of reference label sequence Y given observation sequence X (i.e. P(Y|X))
        
            Args:
                seq_potential: torch tensor, computed sequence potential of shape (seqlen,tagset_size)
                y_labels: list, encoded reference/gold-truth labels of the sequence (i.e. [1,4,3,2,2])
        """
#         print("we are in compute_refscore")
        refscore = autograd.Variable(torch.zeros(1)).type(self.fdtype)
        T = seq_potential.size(0)
#         print("seq_potential: \n", seq_potential)
#         print("y_labels:  \n", y_labels)
        assert T == len(y_labels)
        
        for t in range(T):
            if(t>0):
                curr_tag = y_labels[t]
                prev_tag = y_labels[t-1]
                transition_score = self.y_transparams[prev_tag, curr_tag]    
            else:
                curr_tag = y_labels[t]
                prev_tag = self.y_codebook[self.startstate_symb]
                transition_score = self.y_transparams[prev_tag, curr_tag]
#             print("curr_tag: ", curr_tag)
#             print("prev_tag: ", prev_tag)  
            refscore = refscore + transition_score + seq_potential[t, curr_tag] 
        prev_tag = y_labels[-1]
        curr_tag = self.y_codebook[self.stopstate_symb]
        refscore = refscore + self.y_transparams[prev_tag, curr_tag]
#         print("refscore: \n", refscore)
        return(refscore)

    def decode(self, seq_potential, labels=[], method='viterbi'):
        if(method == 'viterbi'):
            return(self.decode_viterbi(seq_potential))
        elif(method == 'posterior_prob'):
            return(self.decode_posteriorprob(seq_potential))
        elif(method == 'guided_viterbi'):
            return(self.decode_guided(seq_potential, labels))
            
    def decode_posteriorprob(self, seq_potential):
        """ decode a sequence using posterior probability(i.e. using marginal probability of each state at every timestep)
            Args:
                seq_potential: computed sequence potential of shape (seqlen,tagset_size)             
        """
        Z_alpha, alpha = self.compute_forward_vec(seq_potential)
        Z_beta, beta = self.compute_backward_vec(seq_potential)
        P_marginal = self.compute_marginal_prob(alpha, beta, Z_alpha)
        Y_score, Y_decoded = torch.max(P_marginal, dim=1)
        Y_decoded = [self.y_codebookrev[y_code] for y_code in Y_decoded.data[1:-1].tolist()]
        Y_score = Y_score.data[1:-1].tolist()
        return(Y_decoded, Y_score, Z_alpha)
        
    def decode_viterbi(self, seq_potential):
        """ decode a sequence of vector observations to produce the output sequence (X -> Y) using viterbi decoder
        
            Args:
                seq_potential: computed sequence potential of shape (seqlen,tagset_size)
                
            TODO:
                - further cleaning and optimization                
        """
#         print("we are in decode function")
        T = seq_potential.size(0)
        y_codebook = self.y_codebook
        num_rows = T+2
        # create score matrix
        # the score mat is wrapped with Variable to give the possibility for "structured perceptron" training
        score_mat = autograd.Variable(torch.Tensor(num_rows, self.tagset_size).fill_(self.neginf)).type(self.fdtype)
        # back pointer to hold the index of the state that achieved highest score while decoding
        backpointer = torch.Tensor(num_rows, self.tagset_size).fill_(-1).type(self.ldtype)
        score_mat[0, y_codebook[self.startstate_symb]] = 0
#         print("trans_score \n", self.y_transparams)
#         print("score matrix: \n", score_mat)
#         print("backpointer: \n", backpointer)
        for t in range(T):
            for curr_tag in range(self.tagset_size):
#                 print("curr_tag: ", curr_tag)
                emit_score = seq_potential[t, curr_tag]
#                 print("emit_score: \n", emit_score)
                trans_score = self.y_transparams[:, curr_tag]
#                 print("trans_score: \n", trans_score)
                vec = score_mat[t,:] + trans_score + emit_score
#                 print("vec: \n", vec)
                bestscore_tag = argmax_var(vec)
#                 print("best_tag_id: ", bestscore_tag)
                score_mat[t+1, curr_tag] = vec[bestscore_tag]
                backpointer[t+1, curr_tag] = bestscore_tag
#         print("score_matrix: \n", score_mat)
#         print("backpointer: \n", backpointer)
        
        curr_tag = y_codebook[self.stopstate_symb]
#         print("score_mat[{},:]: \n {}".format(num_rows-2, score_mat[num_rows-2,:]))
#         print("self.y_transparams[:,{}]: \n {}".format(curr_tag, self.y_transparams[:,curr_tag]))
        vec = score_mat[num_rows-2,:] + self.y_transparams[:,curr_tag]
#         print("vec: \n", vec)
        t = num_rows-1
#         print("t: ",t)
        bestscore_tag = argmax_var(vec)
#         print("bestscore_tag: ", bestscore_tag)
        score_mat[t, curr_tag] = vec[bestscore_tag]
#         print("score_mat[{}, {}]: \n {}".format(t, curr_tag, score_mat[t, curr_tag]))
        backpointer[t, curr_tag] = bestscore_tag
#         print("backpointer[{}, {}]: \n {}".format(t, curr_tag, backpointer[t, curr_tag]))
        optimalscore = vec[bestscore_tag] # sequence decoding score
#         print("score_matrix: \n", score_mat)
#         print("backpointer: \n", backpointer)

        Y_decoded, Y_score = self._traverse_optimal_seq(score_mat, backpointer, bestscore_tag, t, T)

        return(Y_decoded, Y_score, optimalscore)
    
    def _traverse_optimal_seq(self, score_mat, backpointer, bestscore_tag, last_pointer, T):
        """decoding a sequence based on backpointer and score matrices 
        """
        Y_decoded = [bestscore_tag]
        Y_score = [score_mat[last_pointer-1, bestscore_tag].data[0]]
        counter = 0
#         print("Y_decoded: \n", Y_decoded)
        for t in reversed(range(2, T+1)):
#             print("t: ", t)
#             print("backpointer[{}, {}]={}".format(t, Y_decoded[counter], backpointer[t, Y_decoded[counter]]))
            btag = backpointer[t, Y_decoded[counter]]
#             print("btag: ", btag)
            Y_decoded.append(btag)
            Y_score.append(score_mat[t-1, btag].data[0])
            counter += 1
#             print(btag)
        # re-map the decoded sequence to get the labels
        Y_decoded.reverse()
        Y_decoded = [self.y_codebookrev[y_code] for y_code in Y_decoded]
        Y_score = Y_score[::-1]
#         print("Y_score: \n", Y_score)
#         print("Y_decoded: \n", Y_decoded)
        return(Y_decoded, Y_score)

    def decode_guided(self, seq_potential, labels):
        """ decode **only** the last event of a sequence using viterbi decoder
        
            Args:
                seq_potential: computed sequence potential of shape (seqlen,tagset_size)
            
            TODO:
                - further cleaning and optimization                
        """
#         print("we are in guided decoding")

        T = seq_potential.size(0)
        y_codebook = self.y_codebook
        num_rows = T+2
            
        # create score matrix
        score_mat = autograd.Variable(torch.Tensor(num_rows, self.tagset_size).fill_(self.neginf)).type(self.fdtype)
        # back pointer to hold the index of the state that achieved highest score while decoding
        backpointer = torch.Tensor(num_rows, self.tagset_size).fill_(-1).type(self.ldtype)
        score_mat[0, y_codebook[self.startstate_symb]] = 0

        for t in range(T-1):
            if(t>0):
                prev_tag = labels[t-1]
            else:
                prev_tag = y_codebook[self.startstate_symb]
    
            curr_tag = labels[t]
#             print("prev_tag: ", prev_tag)
#             print("curr_tag: ", curr_tag)
            emit_score = seq_potential[t, curr_tag]
#             print("emit_score: \n", emit_score)
            trans_score = self.y_transparams[prev_tag, curr_tag]
#             print("trans_score: \n", trans_score)
            vec = score_mat[t,prev_tag] + trans_score + emit_score
#             print("vec: \n", vec)
            bestscore_tag = prev_tag
#             print("best_tag_id: ", bestscore_tag)
            score_mat[t+1, curr_tag] = vec[0]
            backpointer[t+1, curr_tag] = bestscore_tag
        
        # decode last event
        t = T - 1
        prev_tag = labels[t-1]
        emit_score = seq_potential[t, :]
        trans_score = self.y_transparams[prev_tag, :]
        vec = score_mat[t,prev_tag] + trans_score + emit_score
        bestscore_tag = prev_tag
        score_mat[t+1, :] = vec
        backpointer[t+1, :] = bestscore_tag 
        
        curr_tag = y_codebook[self.stopstate_symb]
        vec = score_mat[num_rows-2,:] + self.y_transparams[:,curr_tag]
#         print("score_mat[{},:]: \n {}".format(num_rows-2, score_mat[num_rows-2,:]))
#         print("self.y_transparams[:,{}]: \n {}".format(curr_tag, self.y_transparams[:,curr_tag]))
#         print("vec: \n", vec)
        t = num_rows-1
#         print("t: ",t)
        bestscore_tag = argmax_var(vec)
#         print("bestscore_tag: ", bestscore_tag)
#         score_mat[t, curr_tag] = vec[bestscore_tag]
#         print("score_mat[{}, {}] = {}".format(t, curr_tag, score_mat[t, curr_tag]))
        backpointer[t, curr_tag] = bestscore_tag
#         print("backpointer[{}, {}] = {}".format(t, curr_tag, backpointer[t, curr_tag]))

        optimalscore = vec[bestscore_tag] # sequence decoding score
#         print("score_matrix \n", score_mat)
#         print("backpointer \n", backpointer)
        # although we know the decoding of the labels previous to the last event
        # decoding will be done through the whole sequence as of sanity check
        Y_decoded, Y_score = self._traverse_optimal_seq(score_mat, backpointer, bestscore_tag, t, T)
        return(Y_decoded, Y_score, optimalscore)

    
    def compute_loss(self, seq, labels, mode='forward_vectors'):
        """ compute CRF loss
        
            Args:
                seq: variable of size (batch,seqlen,input_dim)
                labels: long tensor corresponding to the labels/tags id
        """
        seq_potential = self.compute_potential(seq)
        if(mode == 'forward_vectors'):
            expected_score, __ = self.compute_forward_vec(seq_potential)
        elif(mode == 'structured_perceptron'):
            __, __, expected_score  = self.decode(seq_potential, method='viterbi')
        refscore  = self.compute_refscore(seq_potential, labels)
        diff = expected_score - refscore
        return(diff)

    def compute_loss_seqpot(self, seq_potential, labels, mode='forward_vectors'):
        """ compute CRF loss using sequence potential as input 
            (i.e. sequence was processed using :func:`self.compute_potential`)
        
            Args:
                seq_potential: 
                labels: long tensor corresponding to the labels/tags id
        """
        if(mode == 'forward_vectors'):
            expected_score, __ = self.compute_forward_vec(seq_potential)
        elif(mode == 'structured_perceptron'):
            __, __, expected_score  = self.decode(seq_potential, method='viterbi')
        refscore  = self.compute_refscore(seq_potential, labels)
        diff = expected_score - refscore
        return(diff)
    
    def forward(self, seq, method='viterbi'): 
        """ perform forward pass
            Args:
                seq: variable of shape (1,seqlen,input_dim)
                
            TODO: implement posterior probability decoding
        """
        seq_potential = self.compute_potential(seq)
        y_decoded, y_score, optimalscore  = self.decode(seq_potential, method=method)
        return(y_decoded, y_score, optimalscore)
    
if __name__ == "__main__":
    pass